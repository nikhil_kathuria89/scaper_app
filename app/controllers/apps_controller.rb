class AppsController < ApplicationController

	require 'rubygems'
    require 'market_bot'

	def index
		@collection = MarketBot::Play::Chart::COLLECTIONS
        @categories = MarketBot::Play::Chart::CATEGORIES
        # debugger
	end

	def search
		@apps = MarketBot::Play::Chart.new(params[:Collection], params[:Categories])
		@apps.update
		# debugger
		respond_to do |format|
			format.js
		end
	end

	def show_details
		@app = MarketBot::Play::App.new(params[:app_id])
		@app.update
		@image = params[:image]
		#debugger
	end
end
